// main.c
//
// Author: Bjorn Melin
// Date: 3/28/2020

#ifdef VIRTUAL_SERIAL
#include <VirtualSerial.h>
#else
#warning VirtualSerial not defined, USB IO may not work
#define SetupHardware();
#define USB_Mainloop_Handler();
#endif
#include "common.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <stdio.h>
#include "timers.h"
#include "tasks.h"
#include "analog.h"



/****************************************************************************
   ALL INITIALIZATION
****************************************************************************/
/**
 * Initializes the system
 */
void init(void) {
	setup_ms_timer();		// initialize the millisecond timer
	initialize_tasks();    	// initialize all 5 tasks
	adc_init();				// initialize ADC for potentiometer
	SetupHardware();       	// This setups the USB hardware and stdio
    sei();                 	// set global interrupt enable - needed for USB
}


/**
 * Function used to start the ms_ticks timer.  Used in 
 * main to time the execution of various tasks.
 */ 
uint32_t get_ms_ticks() {
  cli();
  uint32_t ticks = ms_ticks;	// store curr value of ms_ticks in ticks variable
  sei();						// set global interrupt enable
  return ticks;					// return the current clock time
}


/****************************************************************************
   MAIN
****************************************************************************/
int main(void) {
	USBCON = 0;             // This prevents the need to reset after flashing
	init();    				// call the initialization function
  	sei();                  // set global interrupt enable - needed for USB
	char c;                 // declare char c, which is the key pressed
	uint32_t iterations = 10000;	// denotes max iterations of for loop below

	/********************************************************
	 ********* TIMING EMPTY FOR LOOP ************************
	 *********************************************************/ 
	// start the timer
	uint32_t time = get_ms_ticks();

	uint32_t i;					// initialize counter for for loop

	// Time empty for loop, subtract this time from all tasks times
	for (i = 0; i < iterations; i++) {
		__asm__ __volatile__("nop");	// WCET = 5
	}

	// set the finish time to the current clock time
	// finish = ms_ticks;
	// wcet = (finish - now) / 10000;
	uint32_t loop_time = get_ms_ticks() - time;


	/********************************************************
	 ******** TIMING MULTIPLE ITERATION OF EACH TASK ********
	 *********************************************************/ 

	// start the timer
	uint32_t time2 = get_ms_ticks();
	
	// Run all 3 tasks individually, commenting out all other tasks each time,
	// run each task 10000 times
	for (i = 0; i < iterations; i++) {
		// encoder_task();			// WCET = 5
		// schedule();				// WCET = 20
		potentiometer_task(9);	// WCET = 111
	}

	// calculate average of the task. Calculate & save the worst-case execution
	// time of the task. Subtracts the time taken to run an empty loop.
	uint32_t totalTime = (get_ms_ticks() - time2 - loop_time) / (iterations / 1000);

  	//****** CYCLIC CONTROL LOOP ******//
	// Main while loop
	while(1) {
		USB_Mainloop_Handler();		//Handles USB communication

		// User has pressed a button on the keyboard
    	if ((c = fgetc(stdin)) != EOF) {
			// print the worst-case execution time of the current 
			// task being ran.  WCET is multiplied by 1000 to print as an int
			printf("WCET = %d\n\r", (int) (totalTime));
		}
	}
}