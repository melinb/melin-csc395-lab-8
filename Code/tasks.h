// tasks.h
//
// Author: Bjorn Melin
// Date: 3/31/2020

#ifndef TASKS_H_
#define TASKS_H_

#include <avr/interrupt.h>
#include "inttypes.h"
#include "common.h"
#include <util/delay.h>

// Encoder channels (yellow and white wires) must be plugged in to PB4 and PB5 (Astar pins 8 and 9)
// You can plug in elsewhere, but then this code needs changing.
#define chA_control DDRB
#define chA_pin DDB4
#define chB_control DDRB
#define chB_pin DDB5
// Interrupt #s for encoder are based on where they are plugged in. See AStar pinout PB4 = PCINT4
#define chA_INT PCINT4
#define chB_INT PCINT5


extern volatile int16_t global_counts_m2;
extern volatile int8_t global_error_m2;
// extern volatile uint64_t ms_ticks;


/**
 * Struct used for Tasks
 */
typedef struct task {
	uint8_t ready;
	uint32_t releaseTime;
	uint32_t period;
} TASK_STRUCT;

// Define our 5 Tasks using the TASK_STRUCT
TASK_STRUCT red_Task;
TASK_STRUCT yellow_Task;
TASK_STRUCT buttonA_Task;
TASK_STRUCT key_Task;
TASK_STRUCT printCount_Task;

// Motor encoder task function
void encoder_task(void);

// 5 task scheduler function
void schedule(void);

// Potentiometer task function
uint16_t potentiometer_task(int channel);

// initialize 5 tasks for scheduler function
void initialize_tasks();

#endif