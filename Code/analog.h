// analog.h
//
// Author: Bjorn Melin
// Date: 3/28/2020

// initialize adc
void adc_init();
uint16_t adc_read(uint8_t channel);
