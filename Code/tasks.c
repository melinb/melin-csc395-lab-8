// tasks.c
//
// Author: Bjorn Melin
// Date: 3/31/2020

#include "tasks.h"
#include "timers.h"

volatile int8_t global_m2a;
volatile int8_t global_m2b;

volatile int16_t global_counts_m2 = 0;  // encoder count
volatile int8_t global_error_m2 = 0;

volatile int16_t global_last_m2a_val;
volatile int16_t global_last_m2b_val;
// volatile uint64_t ms_ticks = 0;

/**
 * Initializes all 5 tasks and sets their values
 */
void initialize_tasks() {
    // Define our 5 Tasks using the TASK_STRUCT
    red_Task = (TASK_STRUCT) {0, 0, 500};
    yellow_Task =  (TASK_STRUCT) {0, 0, 250};
    buttonA_Task = (TASK_STRUCT) {0, 0, 1000};
    key_Task = (TASK_STRUCT) {0, 0, 2000};
    printCount_Task = (TASK_STRUCT) {0, 0, 3000};
}


/**
 * Motor Encoder Task Function - converted the motor encoder ISR into 
 * a function so that we can measure execution time from main.c
 */
void encoder_task(void) {
  #ifdef DEBUG
  ++interrupt_counter;
  #endif

  // Make a copy of the current reading from the encoders
  uint8_t tmpB = PINB;

  // Get value of each channel, making it either a 0 or 1 valued integer
  uint8_t m2a_val = (tmpB & (1 << chA_pin )) >> chA_pin;
  uint8_t m2b_val = (tmpB & (1 << chB_pin )) >> chB_pin;

  // Adding or subtracting counts is determined by how these change between interrupts
  int8_t plus_m2 = m2a_val ^ global_last_m2b_val;
  int8_t minus_m2 = m2b_val ^ global_last_m2a_val;

  // Add or subtract encoder count as appropriate
  if(plus_m2) { global_counts_m2 += 1; }
  if(minus_m2) { global_counts_m2 -= 1; }

  // If both values changed, something went wrong - probably missed a reading
  if(m2a_val != global_last_m2a_val && m2b_val != global_last_m2b_val) {
    global_error_m2 = 1;
    PORTD ^= (1 << PORTD5);
  }

  // Save for next interrupt
  global_last_m2a_val = m2a_val;
  global_last_m2b_val = m2b_val;

  // If trying to debug, flash an led so you know the PCINT ISR fired
  #ifdef DEBUG_PCINT
  if (0 == interrupt_counter%20 ) {
    PORTD ^= (1 << PORTD5 );
  }
  #endif
}


/**
 * Scheduler Task Function - converted the scheduler task ISR into 
 * a function so that we can measure execution time from main.c
 */
void schedule(void) {
  // Case where true leads to worst case ex time since all lines run.  
  // Worst case is if all 5 of these execute.
	if (ms_ticks >= red_Task.releaseTime) {
		red_Task.ready = 1;
		red_Task.releaseTime = ms_ticks + red_Task.period;
	}
	else if (ms_ticks >= yellow_Task.releaseTime) {
		yellow_Task.ready = 1;
		yellow_Task.releaseTime = ms_ticks + yellow_Task.period;
	}
	else if (ms_ticks >= buttonA_Task.releaseTime) {
		buttonA_Task.ready = 1;
		buttonA_Task.releaseTime = ms_ticks + buttonA_Task.period;
	}
	else if (ms_ticks >= key_Task.releaseTime) {
		key_Task.ready = 1;
		key_Task.releaseTime = ms_ticks + key_Task.period;
	}
	else if (ms_ticks >= printCount_Task.releaseTime) {
		printCount_Task.ready = 1;
		printCount_Task.releaseTime = ms_ticks + printCount_Task.period;
	}
}


/**
 * Potentiometer Task Function - converted the Potentiometer task ISR 
 * into a function so that we can measure execution time from main.c
 * 
 * Returns: uint16_t - the adc value read by the Potentiometer
 */
uint16_t potentiometer_task(int channel) {
  ADMUX &= 0b11100000;
  if (channel >= 8) {
    ADCSRB |= (1<<MUX5);
    ADMUX |= (channel - 8);
  }
  else {
    ADCSRB &= ~(1<<MUX5);
    ADMUX |= channel;
  }

  // start single conversion
  // write '1' to ADSC
  ADCSRA |= (1<<ADSC);

  // wait for conversion to complete
  // ADSC becomes '0' again
  // till then, run loop continuously
  while(ADCSRA & (1<<ADSC));

  return (ADC);
}